# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2017
# 
# Pierangelo Masarati	<masarati@aero.polimi.it>
# Paolo Mantegazza	<mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
#
# UFO-like model to test HID input and FlightGear communication
#   Andrea Zanoni <andrea.zanoni@polimi.it>

begin: data;
  problem: initial value;
end: data;

include: "./controls/hid.set";
include: "./controls/hid_saitek_x45.set";

set: const real dt = 1e-2;

begin: initial value;
  initial time: 0.;
  final time: 7200.;
  time step: dt;

  real time: posix,
    mode, period, time step, dt*1e9,
    allow nonroot;

	linear solver: naive, colamd;
  max iterations: 20, at most;
end: initial value;

begin: control data;
  air properties;
  structural nodes: 2;
  rigid bodies: 1;
  joints: 
    +1  # ground clamp
    +1  # deformable hinge to limit UFO rotations
    +1  # total pin joint to impose UFO velocities
    ;

  file drivers: 1;
  output elements: 1;
  forces: 
    +1  # "pitch" moment
    +1  # "roll" moment
    +1  # "yaw" moment
    ;
  aerodynamic elements: 
    +1 # aircraft instruments
    ;

  # default output: none;
end: control data;

set: const integer GROUND = 0;
set: const integer UFO = 1;

set: const real kts2mps = 1852./3600.;

set: const real INITIAL_POS_X = 0.;
set: const real INITIAL_POS_Y = 0.;
set: const real INITIAL_ALTITUDE = 30.;

set: const real MAX_LONG_SPEED = 300.*kts2mps;
set: const real MAX_CLIMB_SPEED = 10.;
set: const real MAX_MOMENT = 10.;

module load: "libmodule-flightgear";

reference: UFO,
  reference, global, INITIAL_POS_X, INITIAL_POS_Y, INITIAL_ALTITUDE,
  reference, global, 1, -1., 0., 0., 3, 0., 0., 1., # RF: aeroelasticity
  reference, global, null,
  reference, global, null;

begin: nodes;
  structural: GROUND, static,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;

  structural: UFO, dynamic,
    reference, UFO, null,
    reference, UFO, eye,
    reference, UFO, null,
    reference, UFO, null; 
end: nodes;

set: const integer JOYFLT = 1000;
set: const integer FG_FDM_R = 1001;
begin: drivers;

file: JOYFLT, stream,
  name, "JOYFLT",
  create, no,
  path, "./mbdyn-hid-filt.sock",
  echo, "test/ufo.drv",
  JOYSTICK_N_LC + JOYSTICK_N_BUTTONS;

/*
  file: FG_FDM_R, stream,
    name, "FG_FDM_R",
    	create, no,
    	port, 9012,
      host, "pc-attila",
    	socket type, tcp,
    	non blocking,
    	FlightGear,	
        NetFDM;
*/

end: drivers;

set: const integer UFO_ROTSPRING = 100;
set: const integer UFO_IMPVEL = 105;
set: const integer UFO_MOMENT_X = 110;
set: const integer UFO_MOMENT_Y = 120;
set: const integer UFO_MOMENT_Z = 130;

begin: elements;

  # joint: 0, clamp, GROUND, node, node;

  joint: 0, 
  	total pin joint,
      GROUND,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      # GLOBAL FRAME
        position, reference, other node, null,
        position orientation, reference, other node, eye,
        rotation orientation, reference, other node, eye,
      position constraint, 1, 1, 1,
        # 0., 0., 1., file, FG_FDM_R, FlightGear, agl; 
        null,
      orientation constraint, 1, 1, 1,
        null;

  body: UFO, UFO,
    1., reference, UFO, null, eye;

  joint: UFO_ROTSPRING,
    deformable hinge,
      GROUND,
        orientation, reference, node, eye,
      UFO,
        orientation, reference, node, eye,
      symbolic viscoelastic,
        epsilon, "Phi1", "Phi2", "Phi3",
        epsilon prime, "PhiPrime1", "PhiPrime2", "PhiPrime3",
        expression,
          "0.073751*Phi1 + 0.36876*Phi1^2 + 1.4750*Phi1^3 + 2.*3.*3.1415*PhiPrime1",
          "0.073751*Phi2 + 0.36876*Phi2^2 + 1.4750*Phi2^3 + 2.*3.*3.1415*PhiPrime2",
          "2.*3.*3.1415*PhiPrime3";

  joint: UFO_IMPVEL,
    total pin joint, 
      UFO,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      # GLOBAL FRAME
        position, reference, other node, null,
        position orientation, reference, other node, eye,
        rotation orientation, reference, other node, eye,
      position constraint,
        velocity, velocity, velocity,
          component, 
            file, JOYFLT, JOYSTICK_CYCLIC_LONGITUDINAL, amplitude, MAX_LONG_SPEED/sqrt(2.0),
            file, JOYFLT, JOYSTICK_CYCLIC_LATERAL, amplitude, MAX_LONG_SPEED/sqrt(2.0),
            file, JOYFLT, JOYSTICK_COLLECTIVE, amplitude, MAX_CLIMB_SPEED;

  couple: UFO_MOMENT_Y, follower,
    UFO,
      0., 1., 0., file, JOYFLT, JOYSTICK_CYCLIC_LONGITUDINAL, amplitude, MAX_MOMENT;

  couple: UFO_MOMENT_X, follower,
    UFO,
      1., 0., 0., file, JOYFLT, JOYSTICK_CYCLIC_LATERAL, amplitude, MAX_MOMENT;

  couple: UFO_CONTROL_MOMENT_Z, follower,
    UFO,
      0., 0., 1., file, JOYFLT, JOYSTICK_RUDDER, amplitude, MAX_MOMENT;

  air properties: std, SI, reference altitude, INITIAL_ALTITUDE, null;
  
  # For FlightGear visualization (requires module-flightgear)
  include: "./fg/fg_sim_environ.set";
  
  aircraft instruments: UFO, 
  	UFO, 
		orientation, aeroelasticity,
	  initial latitude, START_LATITUDE,
	  initial longitude, START_LONGITUDE;

  # For Blendyn visualization
  # include: "blendyn_out.elm"; 

  # For FlightGear visualization (requires module-flightgear)
  include: "./fg/fg_out.elm";

end: elements;
