# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2017
# 
# Pierangelo Masarati	<masarati@aero.polimi.it>
# Paolo Mantegazza	<mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
#
# HID input filtering through spring-damper system
#   Andrea Zanoni <andrea.zanoni@polimi.it>

begin: data;
	problem: initial value;
end: data;

set: const real dt = 1.e-2;

begin: initial value;
	initial time: 0.;
	final time: forever;
	time step: dt;
	linear solver: naive, colamd;
  output: none;

end: initial value;

include: "hid.set";
include: "hid_saitek_x45.set";

begin: control data;
  structural nodes: JOYSTICK_N_LC + 1;
  rigid bodies: JOYSTICK_N_LC;
  joints: 2*JOYSTICK_N_LC + 1;
  file drivers: 1;
  output elements: 1;
  forces: JOYSTICK_N_LC;

  default output: none;
end: control data;

module load: "libmodule-hid";

set: const integer JOYSTICK_1 = 1000;

begin: nodes;

  structural: 0, static,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;

  structural: JOYSTICK_CYCLIC_LATERAL, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
  structural: JOYSTICK_CYCLIC_LONGITUDINAL, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
  structural: JOYSTICK_TRIM_CYCLIC_LATERAL, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
  structural: JOYSTICK_PEDALS, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
  structural: JOYSTICK_COLLECTIVE, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
  structural: JOYSTICK_TRIM_CYCLIC_LONGITUDINAL, dynamic,
    reference, global, null,
    reference, global, eye,
    reference, global, null,
    reference, global, null;
    
end: nodes;

# ranges
set: const real CYCLIC_LATERAL_RANGE = 1.;
set: const real CYCLIC_LONGITUDINAL_RANGE = 1.;
set: const real TRIM_CYCLIC_LATERAL_RANGE = .5;
set: const real PEDALS_RANGE = 1.;
set: const real COLLECTIVE_RANGE = -1.;
set: const real TRIM_CYCLIC_LONGITUDINAL_RANGE = .5;

# cut frequencies
set: const real OMEGAC_CYCLIC_LATERAL = 2*pi*10.;
set: const real OMEGAC_CYCLIC_LONGITUDINAL = 2*pi*10.;
set: const real OMEGAC_TRIM_CYCLIC_LATERAL = 2*pi*3.;
set: const real OMEGAC_PEDALS = 2*pi*10.;
set: const real OMEGAC_COLLECTIVE = 2*pi*10.;
set: const real OMEGAC_TRIM_CYCLIC_LONGITUDINAL = 2*pi*3.;

# damping factors
set: const real XI_CYCLIC_LATERAL = 1.0;
set: const real XI_CYCLIC_LONGITUDINAL = 1.0;
set: const real XI_TRIM_CYCLIC_LATERAL = 1.0;
set: const real XI_PEDALS = 1.0;
set: const real XI_COLLECTIVE = 1.0;
set: const real XI_TRIM_CYCLIC_LONGITUDINAL = 1.0;

begin: drivers;
  file: JOYSTICK_1, joystick,
    "/dev/input/js0",
    JOYSTICK_N_BUTTONS,
    JOYSTICK_N_LC,
    scale,
      CYCLIC_LATERAL_RANGE*OMEGAC_CYCLIC_LATERAL^2,
      CYCLIC_LONGITUDINAL_RANGE*OMEGAC_CYCLIC_LONGITUDINAL^2,
      TRIM_CYCLIC_LATERAL_RANGE*OMEGAC_TRIM_CYCLIC_LATERAL^2,
      PEDALS_RANGE*OMEGAC_PEDALS^2,
      COLLECTIVE_RANGE*OMEGAC_COLLECTIVE^2,
      TRIM_CYCLIC_LONGITUDINAL_RANGE*OMEGAC_TRIM_CYCLIC_LONGITUDINAL^2;
end:drivers;

begin: elements;
  body: JOYSTICK_CYCLIC_LATERAL, JOYSTICK_CYCLIC_LATERAL,
    1.0, reference, node, null, eye;

  body: JOYSTICK_CYCLIC_LONGITUDINAL, JOYSTICK_CYCLIC_LONGITUDINAL,
    1.0, reference, node, null, eye;

  body: JOYSTICK_COLLECTIVE, JOYSTICK_COLLECTIVE,
    1.0, reference, node, null, eye;

  body: JOYSTICK_PEDALS, JOYSTICK_PEDALS,
    1.0, reference, node, null, eye;

  body: JOYSTICK_TRIM_CYCLIC_LATERAL, JOYSTICK_TRIM_CYCLIC_LATERAL,
    1.0, reference, node, null, eye;

  body: JOYSTICK_TRIM_CYCLIC_LONGITUDINAL, JOYSTICK_TRIM_CYCLIC_LONGITUDINAL,
    1.0, reference, node, null, eye;

  joint: 0, clamp, 0, node, node;

  joint: JOYSTICK_CYCLIC_LATERAL,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_CYCLIC_LATERAL,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_CYCLIC_LATERAL + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_CYCLIC_LATERAL,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_CYCLIC_LATERAL^2,
        2*XI_CYCLIC_LATERAL*OMEGAC_CYCLIC_LATERAL;

  force: JOYSTICK_CYCLIC_LATERAL + 100, follower,
    JOYSTICK_CYCLIC_LATERAL, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_CYCLIC_LATERAL; 

  joint: JOYSTICK_CYCLIC_LONGITUDINAL,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_CYCLIC_LONGITUDINAL,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_CYCLIC_LONGITUDINAL + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_CYCLIC_LONGITUDINAL,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_CYCLIC_LONGITUDINAL^2,
        2*XI_CYCLIC_LONGITUDINAL*OMEGAC_CYCLIC_LONGITUDINAL;

  force: JOYSTICK_CYCLIC_LONGITUDINAL + 100, follower,
    JOYSTICK_CYCLIC_LONGITUDINAL, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_CYCLIC_LONGITUDINAL; 

  joint: JOYSTICK_TRIM_CYCLIC_LATERAL,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_TRIM_CYCLIC_LATERAL,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_TRIM_CYCLIC_LATERAL + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_TRIM_CYCLIC_LATERAL,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_TRIM_CYCLIC_LATERAL^2,
        2*XI_TRIM_CYCLIC_LATERAL*OMEGAC_TRIM_CYCLIC_LATERAL;

  force: JOYSTICK_TRIM_CYCLIC_LATERAL + 100, follower,
    JOYSTICK_TRIM_CYCLIC_LATERAL, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_TRIM_CYCLIC_LATERAL; 

  joint: JOYSTICK_PEDALS,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_PEDALS,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_PEDALS + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_PEDALS,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_PEDALS^2,
        2*XI_PEDALS*OMEGAC_PEDALS;

  force: JOYSTICK_PEDALS + 100, follower,
    JOYSTICK_PEDALS, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_PEDALS; 

  joint: JOYSTICK_COLLECTIVE,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_COLLECTIVE,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_COLLECTIVE + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_COLLECTIVE,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_COLLECTIVE^2,
        2*XI_COLLECTIVE*OMEGAC_COLLECTIVE;

  force: JOYSTICK_COLLECTIVE + 100, follower,
    JOYSTICK_COLLECTIVE, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_COLLECTIVE; 

  joint: JOYSTICK_TRIM_CYCLIC_LONGITUDINAL,
    total joint,
      0,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      JOYSTICK_TRIM_CYCLIC_LONGITUDINAL,
        position, reference, node, null,
        position orientation, reference, node, eye,
        rotation orientation, reference, node, eye,
      position constraint, 0, 1, 1, null,
      orientation constraint, 1, 1, 1, null;

  joint: JOYSTICK_TRIM_CYCLIC_LONGITUDINAL + 10,
    deformable displacement joint,
      0,
        position, reference, node, null,
      JOYSTICK_TRIM_CYCLIC_LONGITUDINAL,
        position, reference, node, null,
      linear viscoelastic isotropic, 
        OMEGAC_TRIM_CYCLIC_LONGITUDINAL^2,
        2*XI_TRIM_CYCLIC_LONGITUDINAL*OMEGAC_TRIM_CYCLIC_LONGITUDINAL;

  force: JOYSTICK_TRIM_CYCLIC_LONGITUDINAL + 100, follower,
    JOYSTICK_TRIM_CYCLIC_LONGITUDINAL, 
      position, reference, node, null,
    1., 0., 0., file, JOYSTICK_1, JOYSTICK_TRIM_CYCLIC_LONGITUDINAL; 

  stream output: JOYSTICK_1 + 1,
    stream name, "JOYFLT",
    create, yes,
      path, "./mbdyn-hid-filt.sock",
      echo, "./test/hid.sto",
      # socket type, tcp,
      values, JOYSTICK_N_LC + JOYSTICK_N_BUTTONS,
      drive, node, JOYSTICK_CYCLIC_LATERAL, structural, string, "X[1]", direct,
      drive, node, JOYSTICK_CYCLIC_LONGITUDINAL, structural, string, "X[1]", direct,
      drive, node, JOYSTICK_TRIM_CYCLIC_LATERAL, structural, string, "X[1]", direct,
      drive, node, JOYSTICK_PEDALS, structural, string, "X[1]", direct,
      drive, node, JOYSTICK_COLLECTIVE, structural, string, "X[1]", direct,
      drive, node, JOYSTICK_TRIM_CYCLIC_LONGITUDINAL, structural, string, "X[1]", direct,
      drive, file, JOYSTICK_1, JOYSTICK_TRIGGER,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_2,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_3,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_4,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_5,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_6,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_7,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_8,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_9,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_10,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_11,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_12,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_13,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_14,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_15,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_16,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_17,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_18,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_19,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_20,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_21,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_22,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_23,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_24,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_25,
      drive, file, JOYSTICK_1, JOYSTICK_BUTTON_26;
end: elements;
