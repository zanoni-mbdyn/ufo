```
MBDyn (C) is a multibody analysis code. 
http://www.mbdyn.org

Copyright (C) 1996-2017

Pierangelo Masarati     <masarati@aero.polimi.it>
Paolo Mantegazza        <mantegazza@aero.polimi.it>

Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
via La Masa, 34 - 20156 Milano, Italy
http://www.aero.polimi.it

Changing this copyright notice is forbidden.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (version 2 of the License).


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 

```

# ATTILA FRAME--Sim
## Model: UFO

Copyright (C) 2017-2018  
Andrea Zanoni	`<andrea.zanoni@polimi.it>`

This is not strictly a model, it is just a dummy aircraft that can be controlled 
via joystick to roam in the FlightGear environment, for debug purposes. 

### Usage
The repository contains two models that should be co-simulated: the 
`controls/hid_sd_filter.mbd` model and the actual `ufo.mbd` model.

The first thing one should do is set-up the controller he wants to use by editing
the `.set` files in the `controls/` folder and possibly the 
`controls/hid_sd_filter.mbd` file (e.g. to use a device different than the default
one, being 

Then, start the integration of the input filtering model:
```
$ mbdyn ./controls/hid_sd_filter.mbd
```

Start the simulation of the ufo model:

```
$ mbdyn ufo.mbd
```

Then, start FlightGear (parameters are just dummy ones here):
```
fgfs --native-fdm=socket,in,200,,9100,udp --fdm=null
```
